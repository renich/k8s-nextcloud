================
NextCloud en k8s
================
-------------------------------------------
Un intento de espliegue de NextCloud en k8s
-------------------------------------------

Descripción
===========
La idea es generar aquí una manera simple de instalar:

* NextCloud
* Collabora Online con LibreOffice
* Talk + Signaling (turn, nats y janus)

Para hacerlo realmente funcional.


Fases
=====

local
-----
Primero, es echarlo a jalar con minikube.


Instrucciones
=============

Paquetes requeridos
-------------------
* podman
* @virtualization

Requisitos
----------
* Fedora 37
* Ser miembro de los grupos libvirt y qemu (una vez instalado el grupo @virtualization)

Instalación
-----------
Si no tienes configurado el password de `root`, puedes cambiar: ``su -c`` por ``sudo`` y eliminar las comillas.

.. code-block:: sh

    # instalar pre-requisitos (solo si es necesario)
    # especifica tu usuario (normal) para configurarlo
    su -c "./bin/00-pre.bash renich"

    # instalar minikube (solo si es necesario)
    # especifica tu usuario (normal) para configurarlo
    su -c "./bin/01-install.bash renich"

Tras hacer este paso, necesitas correr: ``newgrp qemu`` y ``newgrp libvirt`` para unirte a los grupos requeridos sin tener que salir
de tu sesión (logout). Nomás ten cuidado porque el ``newgrp`` solamente funciona en esa sesión de terminal.

A final de cuentas, es mejor salir y volver a entrar a tu sesión.

.. code-block:: sh

    # configurar minikube
    ./bin/02-configure.bash

    # desplegar
    ./bin/03-deploy.bash

Desinstalación
--------------

.. code-block:: sh

    # plegar
    ./bin/04-undeploy.bash

    # desinstalar
    minikube delete --purge


Referencias
===========
* https://www.haproxy.com/documentation/kubernetes/latest/
* https://github.com/nextcloud/helm/tree/master/charts/nextcloud
* https://github.com/CollaboraOnline/online/tree/master/kubernetes/helm
