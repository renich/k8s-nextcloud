#!/usr/bin/env bash

# Como usuario normal
if [[ ! $( groups $USER ) =~ libvirt ]] || [[ ! $( groups $USER ) =~ qemu ]]; then
	echo 'No eres miembro de libvirt o qemu. Hazte miembro de esos grupos.'
	exit 2
fi

# Configurar
## La mayor cantidad de CPUs que puedas
minikube config set cpus 8

## 16384 MiB de RAM sería mejor
minikube config set memory 16384

## de perdida unos 100 GiB de espacio
minikube config set disk-size 200GiB

## Configúralo para usar el driver de podman... o kvm2
minikube config set driver podman

## Configurarlo para usar crio en vez de docker
## Recuerda cambiarlo a conainerd si no quieres usar sudo sin password para podman
minikube config set bootstrapper kubeadm
minikube config set container-runtime crio

# borrar minikube (para empezar de cero)
minikube delete

# iniciarlo
minikube start

# configurar credenciales de registries
minikube addons configure registry-creds

# obtener IP
echo -n "Configura la siguiente IP dos veces: "
minikube ip

# configurar metallb (con la IP)
minikube addons configure metallb

# activar addons
for addon in volumesnapshots registry-creds metrics-server metallb dashboard csi-hostpath-driver; do
	minikube addons enable $addon;
done

