#!/usr/bin/env bash

# configuración
user=${1:-renich}

# instalar paquetes requeridos
dnf -y install @virtualization podman

# activar servicios
systemctl enable --now libvirtd.socket podman.service

# configurar sudo para podman
cat <<- EOF > /etc/sudoers.d/minikube-renich
$user ALL=(ALL) NOPASSWD: /usr/bin/podman
EOF

# agregar al usuario normal a los grupos requeridos
usermod -aG libvirt,qemu $user

