#!/usr/bin/env bash

# desinstalar nextcloud
helm uninstall --namespace=nextcloud mi-nextcloud

# desinstalar collabora-online
helm uninstall --namespace=collabora mi-collabora-online

# desinstalar el ingress de HAProxy
helm uninstall --namespace=haproxy-controller kubernetes-ingress

