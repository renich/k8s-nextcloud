#!/usr/bin/env bash

# settings
user=${1:-renich}

# check if being run by root
if [[ $( id -ru ) != 0 ]]; then
	echo 'Este script se tiene que correr como root.'
	exit 1
fi

# Como root
## Instalar
dnf -y install https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm

## Haz a tu usuario miembro de qemu y libvirt para
## prevenir facilitar el acceso (menos passwords)
usermod -aG qemu,libvirt $user

## instalar kubectl
## https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-using-native-package-management
cat <<- EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

dnf install -y kubectl

## Por ahora, si queremos usar cri-o, hay que darle poderes de sudo sin password a tu usuario con el comando podman
## Si no, solo cambia el runtime a containerd y jala bien.
## TODO: cambiar a rootless
cat <<- 'EOF' > /etc/sudoers.d/minikube-renich
renich ALL=(ALL) NOPASSWD: /usr/bin/podman
EOF

