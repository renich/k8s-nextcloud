#!/usr/bin/env bash

# instalar el ingress de HAProxy
helm upgrade \
	--install \
	--namespace=haproxy-controller \
	--create-namespace \
	--set controller.service.type=LoadBalancer  \
	kubernetes-ingress haproxytech/kubernetes-ingress

# instalar collabora-online
helm upgrade \
	--install \
	--namespace=collabora \
	--create-namespace \
	--values=collabora-values.yaml \
	mi-collabora-online ~/src/collabora-online/kubernetes/helm/collabora-online/

# instalar nextcloud
helm upgrade \
	--install \
	--namespace=nextcloud \
	--create-namespace \
	--values=nextcloud-values.yaml \
	mi-nextcloud nextcloud/nextcloud

# referencias:
#
# https://www.haproxy.com/documentation/kubernetes/latest/
# https://github.com/nextcloud/helm/tree/master/charts/nextcloud
# https://github.com/CollaboraOnline/online/tree/master/kubernetes/helm
